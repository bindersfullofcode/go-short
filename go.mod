module gitlab.com/bindersfullofcode/go-short

require (
	github.com/go-redis/redis v6.14.1+incompatible
	github.com/teris-io/shortid v0.0.0-20171029131806-771a37caa5cf
)
